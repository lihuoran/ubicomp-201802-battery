\section{Problem Formulation}

\textcolor{red}{As described in the introduction}, the major task of this paper is to propose a model that could predict the battery's usage time before it degrades to a threshold (called \textbf{``target battery level''} in the rest of this paper) that is given by users. In this section, we first define related objects and their notations, then describe the problem definition in a formal way. The summary of notations is listed in Table~\ref{tab:notation}.

\reminder{check notation table}

\input{tables/notation.tex}

\subsection{Discharging Session and Battery Function}

We define a \textit{discharging session} (\textbf{session} for short) as a duration spanning from the beginning of discharging to the end of discharging. In practice, a session corresponds to a continuous period of using time. For example, the time can range from unplugging the charger in the morning to plugging the adapter back before going to bed. In the following parts of this paper, we take session as the basic unit for further analysis. Formally, we use $[t_{start}, t_{end}]$ to denote a session, where $t_{start}$ and $t_{end}$ represent the beginning time and ending time of the session, respectively. Naturally, $t_{end}$ is always larger than $t_{start}$. 

In addition, we employ a function $b(t)$ to represent the battery level of current smartphone at a given time point $t$. In this case, the beginning and ending battery level of a session can be denoted as $b(t_{start})$ and $b(t_{end})$. It is easy to understand that the battery level never goes up within a discharging session, so $b(t)$ monotonically decreases along with the increasing $t$.

\subsection{Battery Life}
\label{sec:low-battery-event-and-battery-lifetime}

When using the device, users may inquire the remaining usage time before the battery degrades to target battery level (denoted as \textit{$battery_{target}$}). This behavior is called a \textit{query} in this study. Let $t_{query}$ represents the time that the user makes the query. Apparently, a query is sense-making if and only if $battery_{target} < b(t_{query})$, which means the target battery level is lower than the current battery level. Let $t_{target}$ represents the time that the battery first goes down to $battery_{target}$. Naturally, $t_{target} > t_{query}$ is always satisfied. Therefore, it is intuitive to define \textit{battery life} as the length of the time period between $t_{query}$ and $t_{target}$. In other words, we can define $life$ as $life=t_{target}-t_{query}$.

However, $t_{target}$ may not always exists in a session. For example, suppose a user leaves home when the phone's battery level is 95\%, and she wants to know when will the battery degrades to 30\%. When she comes back and begin to charge the phone, however, the battery still has 40\% power left. In this case, the battery never truly goes down to the target battery level within this session, so $t_{target}$ is not existed. For target battery level that exists, we call it a \textbf{``observed target''}. Otherwise, we call it a \textbf{``unobserved target''}. How to handle unobserved targets is very critical in practice. For an unobserved target, we could not know the actual lifetime of its corresponding query. When training the prediction model, this data entry will be discarded in traditional methods because they do not have a exact output label. This treatment is risky and will introduce a severe data missing problem, which not only introduces biases into the analysis but also makes the measure of success unreliable. Later in the paper, we will introduce a principled method to deal with the unobserved target battery level. 

\subsection{A Regression Problem}

Based on the definitions above, our goal is to predict $life$ of each query. To be more specific, the prediction target is the remaining time till the battery degrades to the target battery level. Therefore, it is natural to consider such a prediction task as a regression problem. More specifically, given the context of the query, we train a regression model that predicts $life$. The inputs of the regression model contain system status, user's usage behavior, and user's usage history. More details will be discussed in the following sections.
 