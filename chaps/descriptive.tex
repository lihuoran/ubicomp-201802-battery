\section{A Descriptive Analysis}

To demonstrate the representativeness of out data set and to better understand users' battery-usage pattern, we first make a descriptive analysis of our data set in this section. As we have mentioned previously, we take session as the elementary unit during the analysis. Therefore, we first introduce how to generate sessions out of the raw data. 

\subsection{Session Generation}
\label{sec:session-generation}

The battery level data belongs to ``PULL'' data of the Sherlock data set. The data-collection agent reads the battery level of Android OS for approximately every 5 seconds. Each entry of the battery level data contains four fields: 1) user index, 2) timestamp, 3) charging status, and 4) battery percentage. In this case, we can know whether the user's device is charging or discharging along with the battery percentage level at a specific time point. For example, a data entry ``$(0a50e09262, 1426245782, discharge, 54)$'' means that the battery of user ID \textit{0a50e09262} is discharging at the time \textit{1426245782}, and the current battery level is 54\%. 

To generate sessions, we first discard all charging entries, after which we can get a bunch of continuous series of discharging entries. Since the interval between two consecutive entries is only 5 seconds, it is reasonable to trust that the battery keeps discharging between two consecutive discharging entries, unless either of the following two conditions holds:

\begin{enumerate}
    \item The interval between the entries is larger than a large threshold. Such a case could happen as some data are not recorded by the agent. In this case, it cannot be guaranteed that the battery keeps discharging during this period of time. To be more rigorous, we cut one session into two whenever this condition holds. In this work, we set this threshold to be 10 minutes.
    \item The battery level of the latter entry is higher than that of the former one. The explanation of such a phenomenon is that the user replaced a new battery, so the latter entry should be considered as the start of a new session. 
\end{enumerate}

In this way, the original battery-level data could be transformed into some sessions. To make the battery life prediction model reliable and meaningful, we need to further filter out sessions whose lifecycle is too short. For simplicity, we take one hour as the threshold. We believe that sessions shorter than one hour are mostly temporary usage. Their usage pattern is more likely to have a big difference with normal usage, which will affect the prediction's accuracy. Finally, we obtained 37,088 sessions in total. 

% \textcolor{red}{You need to explain why one hour is a reasonable threshold}.

\subsection{Distribution of Battery Usage}

We then report the distributions that can be helpful to validate the representativeness of the data and to motivate the following studies. We start with the distribution of the duration time of the sessions, which is demonstrated in Figure~\ref{fig:cdf-session-duration}. The longest duration is about 71 hours, while the shortest duration is about one hour since we have filtered out sessions that are shorter than this threshold. We can also observe that most sessions are very short. More specifically, 90\% of sessions are shorter than 13 hours, while less than 2\% sessions are longer than one day. Indeed, these results are quite consistent with our common experience. 

\input{figures/cdf-session-duration.tex}

Then, we investigate the distributions of battery usage within each session. The three subfigures in Figure~\ref{fig:cdf-battery} illustrate the distributions of the beginning battery level, the ending battery level, and the battery consumption, respectively. From Figure~\ref{fig:cdf-battery-beginning}, we can see that almost half of the sessions begin when the battery is totally full. This observation indicates that most users are used to completely charging their smartphones before they begin to use. For the rest of the sessions, the distribution of beginning battery level is close to uniform, indicating that users do not have any preferences on the time to begin their usage. 

The distribution of ending battery level is shown in Figure~\ref{fig:cdf-battery-ending}. This curve is pretty close to uniform distribution, too. Hence, we can know that users usually end their usage in different battery levels with similar possibilities. We then break down the two ends of the curve. As for the right end, we can see that about 10\% of the sessions end when the battery is completely full. We infer that these users prefer to keep their batteries fully charged at anytime, and they charge their smartphones whenever they can access available chargers, no matter the battery is full or not. On the other hand, 41\% of the sessions end when the battery level is lower than 20\%, and 5\% of the sessions ``die'', indicating that the battery totally runs out. 

Finally, we focus on the distribution of battery consumption demonstrated in Figure~\ref{fig:cdf-battery-usage}. Here, the battery consumption is defined as the difference between beginning battery level and ending battery level. Shown by the left bottom part of the figure, 10\% of the sessions do not consume any power. This part of sessions is almost the same group with the  sessions that end at the 100\% full status. For the other 90\% of sessions, the distribution of battery consumption is still close to a uniform distribution. In conclusion, the battery consumption of sessions has a large variance.

\input{figures/cdf-session-battery-level.tex}

From the preceding analysis, almost all the observed distributions are consistent with our common-sense knowledge and can be interpreted, which demonstrates the validity of our data set.