\section{Problem Formulation}

One major goal of our study is how to predict battery life based on system status and user behavior. In this section, we first define related objects and their notations, then describe the problem definition. The summary of notations is listed in Table~\ref{tab:notation}.

\input{tables/notation.tex}

\subsection{Discharging Session and Battery Function}

We define a \textit{discharging session} (\textbf{session} for short) as a duration spanning from the beginning of discharging to the end of discharging. In practice, a session corresponds to a continuous period of using time. For example, the time can range from unplugging the charger in the morning to plugging the adapter back before going to bed. In the following parts of this paper, we take session as the basic unit for further analysis. Formally, we use $[t_{start}, t_{end}]$ to denote a session, where $t_{start}$ and $t_{end}$ represent the beginning time and ending time of the session, respectively. Naturally, $t_{end}$ is always larger than $t_{start}$. 

In addition, we employ a function $b(t)$ to represent the battery level of current smartphone at a given time point $t$. In this case, the beginning and ending battery level of a session can be denoted as $b(t_{start})$ and $b(t_{end})$. It is easy to understand that the battery level never goes up within a discharging session, so $b(t)$ monotonically decreases along with the increasing $t$.

\subsection{Low-Battery Event and Battery Life}
\label{sec:low-battery-event-and-battery-lifetime}

We define a session's \textbf{low-battery event} as the first time when the battery level degrades to a threshold $L$ ($0\leq L\leq 100$). Particularly, the low battery event refers to the ``run-out-of-power'' event when $L$ is zero. The time when a low-battery event happens is denoted as $t_{event}$. It is worth mentioning that a low-battery event may not happen if $b(t_{end})>L$, i.e., the session ends before the battery level reaches the threshold. Therefore, sessions can be labelled as \textbf{``observed''} or \textbf{``unobserved''} according to whether the low battery event can be observed within the session. The threshold $L$ could significantly influence the ratio between observed sessions and unobserved sessions. 

Note that how to handle the ``unobserved'' sessions is critical. Since these sessions do not have output labels, most existing work decided to exclude them from the prediction task. This treatment is risky and introduces a severe data missing problem, which not only introduces biases into the analysis but also makes the measure of success unreliable. Later in the paper, we will introduce a principled method to deal with the unobserved sessions. 

For observed sessions, the battery life is defined as the time duration from a given time $t$ to $t_{event}$. This can be denoted as a function $life(t)$, whose definition is $life(t)=t_{event}-t$. It should be noticed that $life(t)$ is meaningful only when $t$ is prior to $t_{event}$. We need to emphasize that the ``battery life'' defined here is more general than the ``physical'' battery life in common sense. According to our definition, a battery runs out of its ``life'' if the battery level degrades below a threshold $L$ (e.g., 10\% or 20\%), rather than the battery being completely drained. The duration to the state that the battery is completely drained is a special case when $L=0$. Intuitively, it is more straightforward to predict the time when the battery is completely empty (i.e. $L=0$). However, such a case  does not frequently occur in real life as most people won't wait until then to recharge their phones. Predicting when the battery would completely die is not only less meaningful in practice, but also more difficult because of fewer training examples. A good selection of the threshold $L$ balances the difficulty and the usefulness of the prediction task. We will discuss more details in the following part of this paper.


% We make this alternative definition of battery life due to the following considerations. 

\subsection{A Regression Problem}

Based on the definitions above, our goal is to predict $life(t)$ from a given time $t$. To be more specific, the prediction target is the remaining time till the battery is considered low (less than the given threshold). It is then natural to consider such a prediction task as a regression problem. More specifically, given any set of information of time $t$, we train a regression model that predicts $life(t)$ for the session. The inputs of the regression model contain system status, user's usage behavior, and user's usage history. More details will be discussed in the following sections. 